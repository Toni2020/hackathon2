from flask import Flask
from flask_cors import CORS, cross_origin
from flask_restplus import Resource, Api
import pandas as pd
import pandas_datareader as pdr
import datetime

app = Flask(__name__)
cors = CORS(app)
api = Api(app)

#get df from yahoo finance
def getDataFrame(ticker):
    df = pdr.get_data_yahoo(ticker, '2000-01-01')
    return df
    
#generate bollinger bands from df
def genBollingerUpper(df):
    df['30d mavg'] = df['Close'].rolling(window=30).mean()
    df['30d std'] = df['Close'].rolling(window=30).std()

    df['Upper Band'] = df['30d mavg'] + (df['30d std'] * 2)
    return df['Upper Band'] 

def genBollingerLower(df):
    df['30d mavg'] = df['Close'].rolling(window=30).mean()
    df['30d std'] = df['Close'].rolling(window=30).std()

    df['Lower Band'] = df['30d mavg'] - (df['30d std'] * 2)
    return df['Lower Band']
            
#check if the current price is above/below bands and return status
def checkStatus(bollinger_upper, bollinger_lower, currentPrice, currentDay):
    #if current price above upper band = sell
    upper_today = bollinger_upper.loc[currentDay]
    print("Upper today:", upper_today)
    if currentPrice > upper_today:
        return 'Sell'
    #if below lower = buy
    lower_today = bollinger_lower.loc[currentDay]
    print("Lower Today:", lower_today)
    if currentPrice < lower_today:
        return 'Buy'
    #if neither = hold
    return 'Hold'
            

@api.route('/<string:ticker>')
class Bollinger(Resource):

    def get(self, ticker):
        df = getDataFrame(ticker)

        currentDay = (datetime.datetime.now() - datetime.timedelta(1)).strftime("%m/%d/%Y")

        currentPrice = df.loc[currentDay]['Adj Close']
        print("Current Price:", currentPrice)

        bollinger_upper = genBollingerUpper(df)
        bollinger_lower = genBollingerLower(df)
        
        status = checkStatus(bollinger_upper, bollinger_lower, currentPrice, currentDay)
        return {
           'ticker' : ticker, 
           'status' : status,
           'date' : currentDay,
           'currentPrice' : currentPrice,
           'sellPrice' : bollinger_upper.loc[currentDay],
           'buyPrice' : bollinger_lower.loc[currentDay]
        }

if __name__=='__main__':
    app.run(debug=True)